# Shared Apartement Manager (SAM)

A management system for a simple, synchronized organization of living together.

<!-- ## **Table of Content**
- [Contributing](#contributing) -->

<!-- ## Install and Run the Project -->

<!-- ## How to Use the Project -->

## Contributing

Thanks for your interest in contributing! There are many ways to contribute to this project. Get started [here](CONTRIBUTING.md).

<!-- ## License -->
